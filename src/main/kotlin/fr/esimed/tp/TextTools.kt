package fr.esimed.tp

import java.util.*

data class Firstname(val sexe:String, val prenom:String, val naissance:String, val age:String)

fun levenshteinDistance(chaine1:String, chaine2:String): Int {
    //chaine1 : lignes chaine2 : colonnes
    val M = Array(chaine1.length + 1){IntArray(chaine2.length + 1){ 0 } }

    for (i in 0..chaine2.length)
        M[0][i] = i

    for (j in 0..chaine1.length)
        M[j][0] = j

    for (i in 1..chaine1.length){
        for (j in 1..chaine2.length){
            var cout = if(chaine1[i - 1] == chaine2[j - 1]) 0 else 1
            M[i][j] = minOf(
                M[i-1][j] + 1,
                M[i][j-1] + 1,
                M[i-1][j-1] + cout
            )
        }
    }
    return M[chaine1.length][chaine2.length]
}

fun percentMatch(chaine1: String, chaine2: String) : Int{
// Calcul du pourcentage de proximité avec les fonctions
//    return 100.minus(levenshteinDistance(chaine1,chaine2).times(100).div(chaine1.length.plus(chaine2.length)))
// Avec les symboles
    return (100 - ((levenshteinDistance(chaine1,chaine2).toFloat() * 100)/(chaine1.length+chaine2.length))).toInt()
}

fun top10 (prenom:String, lesprenoms:MutableSet<String>) : List<String>{
    return lesprenoms.map { it to percentMatch(prenom, it) }.sortedByDescending { it.second }.take(10).map { it.first }
}

fun main(args: Array<String>) {
//    println("Distance de levenshtein entre Arnaud et Julien : ${levenshteinDistance("Arnaud", "Julien")}")
//    println("Distance de levenshtein entre Julie et Julien : ${levenshteinDistance("Julie", "Julien")}")
//
//    println("% match Arnaud Julien : ${percentMatch("Arnaud","Julien")}")
//    println("% match Arnaud Julien : ${percentMatch("Julie","Julien")}")
//    var Top10 = top10("OLIVIER", DataLoader.loadUniqueFirstnames("nat2017.txt")).forEach { println(it) }


}



