package fr.esimed.tp

import sun.text.resources.da.CollationData_da
import java.io.File
import java.util.*

object DataLoader{
    fun loadUniqueFirstnames(fileName:String) : MutableSet<String>{
        var lesprenoms = mutableSetOf<String>()
        File(fileName).forEachLine {
            lesprenoms.add(it.split("\t")[1])
        }
        return lesprenoms
    }

    fun load(path:String):Collection<Firstname>{
        var lesfirstname = arrayListOf<Firstname>()

        File(path).forEachLine {
            var row = it.split("\t")
            lesfirstname.add(Firstname(row[0], row[1], row[2], row[3]))
        }

        return lesfirstname
    }
}

fun main(args: Array<String>) {
//    DataLoader.loadUniqueFirstnames("nat2017.txt").sortedByDescending{ it }.forEach{ println(it) }
//    NOM=[Firstname(_,NOM,_,_),Firstname(_,NOM,_,_),Firstname(_,NOM,_,_)]
    val FirstnameByPrenom = DataLoader.load("nat2017.txt").groupByTo(mutableMapOf()) { it.prenom }

    top10("OLIVIER", DataLoader.loadUniqueFirstnames("nat2017.txt")).forEach {
//        println("$it :")
//
//        FirstnameByPrenom.getValue(it).groupBy { it.naissance }.map { it.key to it.value.count() }.sortedByDescending { it.second }.forEach {
//            println("   ${it.first} : ${it.second}")
//        }
        val plushaut = FirstnameByPrenom.getValue(it).groupBy { it.naissance }.map { it.key to it.value.count() }.maxBy { it.second }

        println("$it : ${if (plushaut != null) "${plushaut.first} , ${plushaut.second}" else "Aucune donnée" }")
    }





}