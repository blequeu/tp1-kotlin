package fr.esimed.tp.test

import fr.esimed.tp.DataLoader
import fr.esimed.tp.levenshteinDistance
import fr.esimed.tp.percentMatch
import fr.esimed.tp.top10
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.Executable


class TextToolsTests{
    @Test
    fun levenshteinDistanceTest(){
        Assertions.assertAll(
            Executable { Assertions.assertEquals(6, levenshteinDistance("Arnaud", "Julien")) },
            Executable { Assertions.assertEquals(1, levenshteinDistance("Julie", "Julien")) },
            Executable { Assertions.assertEquals(50, percentMatch("Arnaud", "Julien")) },
            Executable { Assertions.assertEquals(90, percentMatch("Julie","Julien")) }

        )
    }

    @Test
    fun DataLoaderTest(){
        Executable {
            Assertions.assertEquals(
                listOf("OLIVIER", "OLIVIERO", "OLLIVIER", "LIVIER", "OLIVER", "OLIVIEN", "OLIWIER", "OLIVERA", "OLIVINE", "OLIVIO"),
                top10("OLIVIER", DataLoader.loadUniqueFirstnames("nat2017.txt"))
            )
        }
    }
}


